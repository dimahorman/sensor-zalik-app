package service

import io.ktor.client.*
import io.ktor.client.engine.cio.*
import io.ktor.client.request.*
import util.Secrets
import java.lang.RuntimeException

class ApiService {
    private val client = HttpClient(CIO)
    private val sensorId = Secrets.get("SENSOR_ID") ?: throw RuntimeException("Please define value of SENSOR_ID")
    private val url = Secrets.get("FUNCTION_URL") ?: throw RuntimeException("Please define value of FUNCTION_URL")

    suspend fun pushMeasurements(value: String): String {
        return client.get("$url/httpexample?sensorId=$sensorId&value=$value")
    }
}
